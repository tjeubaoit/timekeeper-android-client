package org.lib.anhtn.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import java.io.IOException;
import java.lang.ref.WeakReference;

public abstract class AbstractGcmRegister {

    public static interface ResultCallback {

        public void onSuccess();
        public void onFailure();
    }

    private static class BackendResponseHandler extends Handler {

        private final WeakReference<AbstractGcmRegister> mGcmRegister;

        public BackendResponseHandler(AbstractGcmRegister register) {
            mGcmRegister = new WeakReference<>(register);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            ResultCallback callback = mGcmRegister.get().mCallback;
            if (callback != null) {
                if (msg.what == SUCCESS_MESSAGE) {
                    callback.onSuccess();
                } else if (msg.what == FAILURE_MESSAGE) {
                    callback.onFailure();
                }
            }
        }
    }

    private static final String REGISTRATION_ID_KEY = "gcm.registration_id";
    private static final String APP_VERSION_KEY = "app.version";
    private static final String BACKEND_RECEIVED_GCM_ID_KEY = "backend.received.gcm_id";
    private static final String TAG = "GCM-REGISTER-DEBUG";

    private static final int SUCCESS_MESSAGE = 200;
    private static final int FAILURE_MESSAGE = 501;

    private Context mContext;
    private String mRegId, mSenderId;
    private ResultCallback mCallback;
    private BackendResponseHandler mBackendResponseHandler;

    /**
     * @param context Context where this task run in
     * @param senderId Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    public AbstractGcmRegister(Context context, String senderId) {
        mContext = context;
        mSenderId = senderId;
        mBackendResponseHandler = new BackendResponseHandler(this);
    }

    /**
     * Do async register a gcm registration id in background
     * @param callback Callback result of register
     */
    public final void asyncRegister(ResultCallback callback) {
        if (Looper.myLooper() == null) {
            throw new IllegalStateException("Current thread not associated with a looper");
        } else mCallback = callback;

        mRegId = getRegistrationIdInPreference();
        if (mRegId.isEmpty()) {
            Log.e(TAG, "Start register new ID with GCM Server");
            registerInBackground();
        }
        else if (!getFlagBackendReceivedRegIdInPreference()) {
            Log.e(TAG, "RegistrationId exist, but backend server not have it");
            sendRegistrationIdToBackendInBackground();
        }
    }

    public final Context getContext() {
        return mContext;
    }

    public final String getRegistrationId() {
        return mRegId;
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    protected abstract boolean sendRegistrationIdToBackend();

    /**
     * Gets the current registration ID for application on GCM service.
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationIdInPreference() {
        final SharedPreferences prefs = getDefaultPreferences(mContext);
        String registrationId = prefs.getString(REGISTRATION_ID_KEY, "");
        if (registrationId.isEmpty()) {
            Log.e(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(APP_VERSION_KEY, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(mContext);
        if (registeredVersion != currentVersion) {
            Log.e(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private boolean getFlagBackendReceivedRegIdInPreference() {
        final SharedPreferences prefs = getDefaultPreferences(mContext);
        return prefs.getBoolean(BACKEND_RECEIVED_GCM_ID_KEY, false);
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mContext);
                    mRegId = gcm.register(mSenderId);
                    Log.e(TAG, "Device registered, registration ID=" + mRegId);

                    // For this demo: we don't need to send it because the device
                    // will send upstream messages to a server that echo back the
                    // message using the 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(mContext, mRegId);

                    // You should send the registration ID to your server over HTTP,
                    // so it can use GCM/HTTP or CCS to send messages to your app.
                    // The request to your server should be authenticated if your app
                    // is using accounts.
                    if (sendRegistrationIdToBackend()) {
                        storeFlagBackendReceivedRegId(getContext(), true);
                        mBackendResponseHandler.sendEmptyMessage(SUCCESS_MESSAGE);
                    } else {
                        mBackendResponseHandler.sendEmptyMessage(FAILURE_MESSAGE);
                    }
                } catch (IOException ex) {
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                    Log.e(TAG, "Error :" + ex.getMessage());
                }
            }
        }).start();
    }

    private void sendRegistrationIdToBackendInBackground() {
        if (Looper.myLooper() == Looper.getMainLooper()) { // is UI thread
            Log.e(TAG, "Current in UI thread, send registrationId in other thread");
            new Thread(new Runnable() {
                @Override
                public void run() {
                    if (sendRegistrationIdToBackend()) {
                        mBackendResponseHandler.sendEmptyMessage(SUCCESS_MESSAGE);
                    } else {
                        mBackendResponseHandler.sendEmptyMessage(FAILURE_MESSAGE);
                    }
                }
            }).start();
        } else {
            Log.e(TAG, "Current in background thread, send registrationId in this thread");
            if (sendRegistrationIdToBackend()) {
                mBackendResponseHandler.sendEmptyMessage(SUCCESS_MESSAGE);
            } else {
                mBackendResponseHandler.sendEmptyMessage(FAILURE_MESSAGE);
            }
        }
    }

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private static void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getDefaultPreferences(context);
        int appVersion = getAppVersion(context);
        Log.e(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(REGISTRATION_ID_KEY, regId);
        editor.putInt(APP_VERSION_KEY, appVersion);
        editor.apply();
    }

    private static void storeFlagBackendReceivedRegId(Context context, boolean value) {
        final SharedPreferences prefs = getDefaultPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(BACKEND_RECEIVED_GCM_ID_KEY, value);
        editor.apply();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private static SharedPreferences getDefaultPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}


