package com.vtek.timekeeper.app;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

import com.vtek.timekeeper.R;
import com.vtek.timekeeper.RealTimeActivity;
import com.vtek.timekeeper.model.GcmRegister;
import com.vtek.timekeeper.model.Person;
import com.vtek.timekeeper.utils.GlobalFunctions;
import com.vtek.timekeeper.utils.GlobalValues;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TimeKeeperBackgroundService extends Service {

    private BroadcastReceiver mGcmReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            final Bundle bundle = intent.getBundleExtra("bundle");
            final String collapseKey = bundle.getString("collapse_key");

            if (GlobalValues.GCM_COLLAPSE_KEY_REALTIME_UPDATE.equals(collapseKey)) {
                Person person = new Person();
                person.ID = Integer.parseInt(bundle.getString("id"));
                person.Code = bundle.getString("code");
                person.Name = bundle.getString("name");
                person.ViolationCount = Integer.parseInt(bundle.getString("violation_count"));
                person.WorkState = bundle.getString("work_state");
                person.LastScan = bundle.getString("last_scan");
                person.Birthday = bundle.getString("birthday");

                if (!updatePerson(person)) return;

                // if RealTimeActivity not in foreground, send a notification
                if (RealTimeActivity.isActivityVisible()) {
                    Intent i = new Intent(GlobalValues.ACTION_BACKGROUND_DATA_CHANGED);
                    i.putExtra("person_ids", new int[] { person.ID });
                    LocalBroadcastManager.getInstance(
                            TimeKeeperBackgroundService.this).sendBroadcast(i);
                } else {
                    if (!mPersonIdsNeedUpdateInUi.add(person.Code)) return;

                    Intent i = new Intent(context, RealTimeActivity.class);
                    intent.putExtra("sender", "notification_manager");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent contentIntent = PendingIntent.getActivity(context, 0, i,
                            PendingIntent.FLAG_UPDATE_CURRENT);

                    final int size = mPersonIdsNeedUpdateInUi.size();
                    final String msg;
                    if (size > 1) {
                        msg = getResources().getString(
                                R.string.real_time_multi_updated, size);
                    } else {
                        final String name = person.Name;
                        if (name == null || name.isEmpty()) return;
                        msg = getResources().getString(
                                R.string.real_time_single_updated, name);
                    }

                    GlobalFunctions.sendNotification(context,
                            getResources().getString(R.string.app_name),
                            msg, RealTimeActivity.NOTIFICATION_ID, contentIntent, null);
                }
            }
        }
    };

    public class LocalBinder extends Binder {

        public TimeKeeperBackgroundService getService() {
            return TimeKeeperBackgroundService.this;
        }
    }
    private final IBinder mBinder = new LocalBinder();

    private List<Person> mPersons = new ArrayList<>();
    private Set<String> mPersonIdsNeedUpdateInUi = new HashSet<>();

    @Override
    public void onCreate() {
        super.onCreate();
        GcmRegister.getInstance(this).asyncRegister(null);

        IntentFilter gcmFilter =
                new IntentFilter(GlobalValues.ACTION_GCM_TO_BACKGROUND);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mGcmReceiver, gcmFilter);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        GlobalFunctions.log("Background service onBind");
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        GlobalFunctions.log("Background service onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mGcmReceiver);
    }

    public boolean isDataInitialized() {
        return !mPersons.isEmpty();
    }

    public void updateNewPersonList(List<Person> persons) {
        mPersons.clear();
        mPersons.addAll(persons);
    }

    public boolean updatePerson(Person person) {
        int index = -1;
        for (int i = 0; i < mPersons.size(); i++) {
            if (mPersons.get(i).ID == person.ID) {
                index = i;
                break;
            }
        }
        if (index >= 0) {
            mPersons.set(index, person);
            return true;
        }
        return false;
    }

    public Person findPersonById(int id) {
        for (Person p : mPersons) {
            if (p.ID == id) return p;
        }
        return null;
    }

    public List<Person> findPersonByName(String name) {
        List<Person> result = new ArrayList<>();
        for (Person p : mPersons) {
            if (p.Name.contains(name)) {
                result.add(p);
            }
        }
        return result;
    }

    public List<Person> getPersonList() {
        return mPersons;
    }

    public void setAllItemIsUpdatedInUi() {
        mPersonIdsNeedUpdateInUi.clear();
    }

    public boolean hasDataNeedUpdateInUi() {
        return !mPersonIdsNeedUpdateInUi.isEmpty();
    }
}
