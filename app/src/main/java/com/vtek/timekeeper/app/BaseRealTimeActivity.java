package com.vtek.timekeeper.app;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vtek.timekeeper.R;
import com.vtek.timekeeper.content.RealTimeRecyclerAdapter;
import com.vtek.timekeeper.view.IViewTranslation;
import com.vtek.timekeeper.view.MarginViewTranslationInFrameLayout;
import com.vtek.timekeeper.view.SystemViewTranslationCompat;

import org.lib.anhtn.navigation.AbstractNavigationDrawerActivity;
import org.lib.anhtn.navigation.BaseNavigationDrawerFragment;


public abstract class BaseRealTimeActivity extends AbstractNavigationDrawerActivity
        implements RealTimeRecyclerAdapter.OnItemClickListener {

    private BaseNavigationDrawerFragment mNavigationDrawerFragment;
    private LinearLayoutManager linearLayoutManager;
    protected RealTimeRecyclerAdapter mAdapter;
    protected RecyclerView recyclerView;


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_time);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new RealTimeRecyclerAdapter(this, this);
        recyclerView.setAdapter(mAdapter);
//        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
//
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
//                    final float translateY = mToolbarTranslation.getTranslationY();
//                    final float scale = Math.abs(translateY) / getActionBarSize();
//                    if (scale < 0.5f && scale > 0) {
//                        mToolbarTranslation.setTranslationYWithAnimation(0, 200);
//                    } else if (scale > 0.5 && scale < 1) {
//                        mRecyclerTranslation.setTranslationYWithAnimation(
//                                (-1) * getActionBarSize(), 200);
//                        mToolbarTranslation.setTranslationYWithAnimation(
//                                (-1) * getActionBarSize(), 200);
//                    }
//                } else if (newState == RecyclerView.SCROLL_STATE_SETTLING) {
//
//                } else {
//
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                recyclerViewScrollOnTop = recyclerView.getChildAt(0).getTop() >= 15;
//                int lastId = linearLayoutManager.findLastCompletelyVisibleItemPosition();
//
//                if (lastId != RecyclerView.NO_POSITION
//                        && lastId == recyclerView.getChildCount() - 1) {
////                    mToolbarTranslation.setTranslationYWithAnimation(0, 200);
//                } else if (recyclerViewScrollOnTop) {
//                    mToolbarTranslation.setTranslationYWithAnimation(0, 200);
//                } else {
//                    float y = mToolbarTranslation.getTranslationY() - dy;
//                    //                GlobalFunctions.log(dy + "," + y);
//                    if (y > 0) y = 0;
//                    if (y < (-1) * getActionBarSize()) y = (-1) * getActionBarSize();
//                    mToolbarTranslation.setTranslationY(y);
//                }
//            }
//        });

//        final TouchInterceptionFrameLayout rootView =
//                (TouchInterceptionFrameLayout) findViewById(R.id.real_time_container);
//
//        rootView.setScrollInterceptionListener(
//                new TouchInterceptionFrameLayout.TouchInterceptionListener() {
//            @Override
//            public boolean shouldInterceptTouchEvent(MotionEvent motionEvent, boolean moving,
//                                                     float diffX, float diffY) {
//                boolean ok = false;
//
//                if (diffY < 0) {
//                    float dy = diffY - lastDiffY;
//                    if (dy + mRecyclerTranslation.getTranslationY() > (-1)*getActionBarSize())
//                        ok = true;
//                } else if (diffY > 0 && recyclerViewScrollOnTop) {
//                    ok = true;
//                }
//                if (!ok) lastDiffY = 0;
//                return ok;
//            }
//
//            @Override
//            public void onDownMotionEvent(MotionEvent motionEvent) {
//
//            }
//
//            @Override
//            public void onMoveMotionEvent(MotionEvent motionEvent, float diffX, float diffY) {
//                float dy = diffY - lastDiffY + mToolbarTranslation.getTranslationY();
//                if (dy > 0) dy = 0;
//                if (dy < (-1) * getActionBarSize()) dy = (-1) * getActionBarSize();
//                mToolbarTranslation.setTranslationY(dy);
//
//                dy = diffY - lastDiffY + mRecyclerTranslation.getTranslationY();
//                if (dy > 0) dy = 0;
//                if (dy < (-1) * getActionBarSize()) dy = (-1) * getActionBarSize();
//                mRecyclerTranslation.setTranslationY(dy);
//
//                lastDiffY = diffY;
//            }
//
//            @Override
//            public void onUpOrCancelMotionEvent(MotionEvent motionEvent) {
//                lastDiffY = 0;
//                final float dy = mToolbarTranslation.getTranslationY();
//                if (Math.abs(dy) / getActionBarSize() < 0.5f) {
//                    mToolbarTranslation.setTranslationYWithAnimation(0, 200);
//                } else {
//                    mToolbarTranslation.setTranslationYWithAnimation(
//                            (-1) * getActionBarSize(), 200);
//                    mRecyclerTranslation.setTranslationYWithAnimation(
//                            (-1) * getActionBarSize(), 200);
//                }
//            }
//        });
        mRecyclerTranslation = new MarginViewTranslationInFrameLayout(recyclerView);
        mToolbarTranslation = new SystemViewTranslationCompat(getToolbar());
    }

    private IViewTranslation mRecyclerTranslation, mToolbarTranslation;
    private float lastDiffY = 0;
    private boolean recyclerViewScrollOnTop = true;

    private int getActionBarSize() {
        return getResources().getDimensionPixelSize(
                R.dimen.action_bar_default_height_material);
    }

    @Override
    protected BaseNavigationDrawerFragment getNavigationDrawerFragment() {
        if (mNavigationDrawerFragment == null) {
            mNavigationDrawerFragment = new NavigationDrawerFragment();
        }
        return mNavigationDrawerFragment;
    }
}
