package com.vtek.timekeeper.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vtek.timekeeper.R;
import com.vtek.timekeeper.utils.GlobalFunctions;

import org.lib.anhtn.navigation.BaseNavigationDrawerFragment;

public class NavigationDrawerFragment extends BaseNavigationDrawerFragment {

    /**
     * Remember the position of the selected item.
     */
    private static final String SELECTED_POSITION = "list.selected.position";
    private static final int INVALID_POSITION = -1;

    private NavigationEventListener mNavEventListener;
    private ListView mDrawerListView;
    private int mCurrentSelectedPosition = INVALID_POSITION;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(SELECTED_POSITION);
            selectItemAt(mCurrentSelectedPosition);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        mDrawerListView = (ListView) inflater.inflate(
                R.layout.fragment_navigation_drawer, container, false);
        mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0 && position != 5) {
                    selectItemAt(position);
                }
            }
        });
        mDrawerListView.setAdapter(new NavArrayAdapter(getActivity(),
                R.layout.fragment_navigation_drawer,
                mNavEventListener.getDefaultSelectedId()));
        mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);

        return mDrawerListView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mNavEventListener = (NavigationEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement ItemSelectListener.");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mNavEventListener != null) {
            selectItemAt(mNavEventListener.getDefaultSelectedId());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mNavEventListener = null;
    }

    private void selectItemAt(int position) {
        if (mDrawerListView != null) {
            mDrawerListView.setItemChecked(position, true);
            mDrawerListView.setSelection(position);

            final View selectedView = mDrawerListView.getChildAt(position);
            if (selectedView != null) {
                selectedView.setSelected(true);
                NavArrayAdapter.setItemBackground(selectedView, R.color.vtek_grey200);
            }

            if (position != mCurrentSelectedPosition) {
                final View oldView = mDrawerListView.getChildAt(mCurrentSelectedPosition);
                if (oldView != null) {
                    NavArrayAdapter.setItemBackground(oldView,
                            android.R.color.transparent);
                }
            }
        }

        DrawerLayout drawerLayout = getDrawerLayout();
        if (drawerLayout != null) {
            drawerLayout.closeDrawer(getFragmentContainerView());
        }
        if (mNavEventListener != null) {
            mNavEventListener.onNavigationItemSelected(position);
        }
        mCurrentSelectedPosition = position;
    }

    private static class NavArrayAdapter extends ArrayAdapter<Integer> {

        private static final int VIEW_TYPE_COVER = 0;
        private static final int VIEW_TYPE_ACTION = 1;
        private static final int VIEW_TYPE_SEPARATOR = 2;

        private static final int[] IconResources = {
                0,
                R.drawable.time,
                R.drawable.report,
                R.drawable.management,
                R.drawable.account,
                0,
                R.drawable.setting,
                R.drawable.help,
                R.drawable.about
        };

        private final int mDefaultSelectedPosition;

        public NavArrayAdapter(Context context, int resource, int defaultPos) {
            super(context, resource);
            for (int resourceId : IconResources) {
                add(resourceId);
            }
            mDefaultSelectedPosition = defaultPos;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View rootView = convertView;
            if (rootView != null) {
                return rootView;
            }

            final LayoutInflater inflater = (LayoutInflater)
                    getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final int viewType = getItemViewType(position);
            final Context context = getContext();

            if (viewType == VIEW_TYPE_COVER) {
                rootView = inflater.inflate(R.layout.view_navigation_type_cover,
                        parent, false);
                rootView.setEnabled(false);
                final TextView txtUserName = (TextView)
                        rootView.findViewById(R.id.nav_text_username);
                txtUserName.setText(GlobalFunctions.getCurrentUserNameAuthenticated(context));
            }
            else if (viewType == VIEW_TYPE_SEPARATOR) {
                rootView = inflater.inflate(R.layout.view_list_separator_horizontal,
                        parent, false);
                rootView.setEnabled(false);
            }
            else {
                rootView = inflater.inflate(R.layout.view_navigation_type_item,
                        parent, false);
                if (position == mDefaultSelectedPosition) {
                    setItemBackground(rootView, R.color.vtek_grey200);
                }

                final TextView txtNavItem = (TextView)
                        rootView.findViewById(R.id.nav_text_action);
                final String[] strings = context
                        .getResources().getStringArray(R.array.nav_item_text);
                txtNavItem.setText(strings[position]);

                final ImageView imgNavIcon = (ImageView)
                        rootView.findViewById(R.id.nav_img_icon);
                imgNavIcon.setImageResource(getItem(position));

//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                    txtNavItem.setCompoundDrawablesRelativeWithIntrinsicBounds(
//                            getItem(position), 0, 0, 0);
//                } else {
//                    txtNavItem.setCompoundDrawablesWithIntrinsicBounds(
//                            getItem(position), 0, 0, 0);
//                }
            }
            return rootView;
        }

        @Override
        public int getItemViewType(int position) {
            switch (position) {
                case 0: return VIEW_TYPE_COVER;
                case 5: return VIEW_TYPE_SEPARATOR;
                default: return VIEW_TYPE_ACTION;
            }
        }

        @Override
        public int getViewTypeCount() {
            return 3;
        }

        public static void setItemBackground(View rootView, int resId) {
//            rootView.setBackgroundResource(resId);
            final View view = rootView.findViewById(R.id.nav_item_background);
            view.setBackgroundResource(resId);
        }
    }

    /**
     * interface that all activities using this fragment must implement.
     */
    public static interface NavigationEventListener {

        public static final int REAL_TIME_ITEM_ID = 1;
        public static final int REPORT_ITEM_ID = 2;
        public static final int MANAGEMENT_ITEM_ID = 3;
        public static final int ACCOUNT_ITEM_ID = 4;
        public static final int SETTING_ITEM_ID = 6;
        public static final int HELP_FEEDBACK_ITEM_ID = 7;
        public static final int ABOUT_ITEM_ID = 8;

        /**
         * Called when an item in the navigation drawer is selected.
         */
        public void onNavigationItemSelected(int id);

        /**
         * Called when navigation drawer resume
         * @return default selected id for activity that using this fragment
         */
        public int getDefaultSelectedId();
    }
}
