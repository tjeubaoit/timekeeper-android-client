package com.vtek.timekeeper.app;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.vtek.timekeeper.utils.GlobalValues;

public class GcmIntentService extends IntentService {

    private static final String TAG = "GCM-DEBUG";

    public GcmIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Log.e(TAG, "Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                Log.e(TAG, "Deleted messages on server: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // Post notification of received message.
                Log.e(TAG, "Received: " + extras.toString());
                sendBroadcastToConsumers(extras);
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        WakefulBroadcastReceiver.completeWakefulIntent(intent);
    }

    private void sendBroadcastToConsumers(Bundle extras) {
        final String collapseKey = extras.getString("collapse_key");
        if (GlobalValues.GCM_COLLAPSE_KEY_REALTIME_UPDATE.equals(collapseKey)) {
            Intent i = new Intent(GlobalValues.ACTION_GCM_TO_BACKGROUND);
            i.putExtra("bundle", extras);
            LocalBroadcastManager.getInstance(this).sendBroadcast(i);
        }
    }
}
