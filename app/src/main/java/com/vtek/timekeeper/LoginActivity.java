package com.vtek.timekeeper;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.vtek.timekeeper.model.LoginHelper;
import com.vtek.timekeeper.utils.GlobalFunctions;
import com.vtek.timekeeper.utils.GlobalValues;


public class LoginActivity extends ActionBarActivity implements LoginHelper.Callback {

    private ProgressBar pbLogin;
    private LinearLayout layoutLogin;
    private ImageView imgLogo;
    private TextView txtForgotPass;
    private EditText editName, editPassword;

    private static final int MIN_LENGTH = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final Button btnLogin = (Button) findViewById(R.id.button_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final String username = editName.getText().toString();
                if (username.isEmpty() || username.length() < MIN_LENGTH) {
                    showToastLoginFailure(R.string.login_name_field_invalid);
                    return;
                }

                final String password = editPassword.getText().toString();
                if (password.isEmpty() || password.length() < MIN_LENGTH) {
                    showToastLoginFailure(R.string.login_pass_field_invalid);
                    return;
                }

                if (!GlobalFunctions.checkNetworkAvailable(LoginActivity.this)) {
                    GlobalFunctions.log("Network connection not available");
                    showToastLoginFailure(R.string.network_invalid);
                    return;
                }

                LoginHelper loginHelper = new LoginHelper(username, password,
                        LoginActivity.this);
                loginHelper.asyncLogin(LoginActivity.this);
            }
        });

        editName = (EditText) findViewById(R.id.edit_username);
        editPassword = (EditText) findViewById(R.id.edit_password);
        editPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnLogin.performClick();
                }
                return false;
            }
        });

        pbLogin = (ProgressBar) findViewById(R.id.progress_login);
        layoutLogin = (LinearLayout) findViewById(R.id.login_form);
        imgLogo = (ImageView) findViewById(R.id.image_logo);
        txtForgotPass = (TextView) findViewById(R.id.text_forgot_pass);
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (layoutLogin.getVisibility() != View.VISIBLE) {
            startAnimation();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, android.R.anim.slide_out_right);
    }

    @Override
    public void onPrepareLogin() {
        pbLogin.setVisibility(View.VISIBLE);
        layoutLogin.setVisibility(View.INVISIBLE);
        txtForgotPass.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onLoginSuccess(String user, String pass) {
        SharedPreferences.Editor editor =
                getSharedPreferences(GlobalValues.APP_PREF_NAME, MODE_PRIVATE).edit();
        editor.putString(GlobalValues.USER_KEY, user);
        editor.putString(GlobalValues.PASSWORD_KEY, pass);
        editor.putBoolean(GlobalValues.AUTHENTICATED_KEY, true);
        editor.apply();

        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onLoginFailure(String user, String pass) {
        pbLogin.setVisibility(View.INVISIBLE);
        layoutLogin.setVisibility(View.VISIBLE);
        txtForgotPass.setVisibility(View.VISIBLE);
//        editPassword.setText("");

        showToastLoginFailure(R.string.login_result_fail);
        SharedPreferences.Editor editor =
                getSharedPreferences(GlobalValues.APP_PREF_NAME, MODE_PRIVATE).edit();
        editor.clear().apply();
    }

    private void showToastLoginFailure(int stringId) {
        Toast.makeText(this, stringId, Toast.LENGTH_SHORT).show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void startAnimation() {
        AnimatorSet logoTranslateAnimSet = (AnimatorSet) AnimatorInflater.loadAnimator(
                this, R.animator.login_logo_translate);
        for (Animator animator : logoTranslateAnimSet.getChildAnimations()) {
            try {
                ValueAnimator valueAnimator = (ValueAnimator) animator;
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        RelativeLayout.MarginLayoutParams lp =
                                (RelativeLayout.MarginLayoutParams) imgLogo.getLayoutParams();
                        lp.bottomMargin = (Integer) animation.getAnimatedValue();
                        imgLogo.setLayoutParams(lp);
                    }
                });
            } catch (ClassCastException ex) {
                GlobalFunctions.log(ex);
            }
        }
        logoTranslateAnimSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                layoutLogin.setVisibility(View.VISIBLE);
                txtForgotPass.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        logoTranslateAnimSet.start();
    }
}
