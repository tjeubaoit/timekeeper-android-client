package com.vtek.timekeeper;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.vtek.timekeeper.utils.RestClient;
import com.vtek.timekeeper.model.Person;
import com.vtek.timekeeper.utils.GlobalFunctions;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class RealTimePersonDetailActivity extends ActionBarActivity
        implements View.OnClickListener {

    public static final String PERSON_KEY = "com.vtek.timekeeper.person";
    public static final String ACTION_KEY = "com.vtek.timekeeper.action";

//    private static class ViolationDetailArrayAdapter extends ArrayAdapter<ViolationDetail> {
//
//        public ViolationDetailArrayAdapter(Context context, int resource) {
//            super(context, resource);
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            View rootView = convertView;
//            if (rootView == null) {
//                LayoutInflater inflater = (LayoutInflater)
//                        getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                rootView = inflater.inflate(R.layout.view_real_time_detail_item,
//                        parent, false);
//            }
//
//            final TextView txtWorkTime =
//                    (TextView) rootView.findViewById(R.id.text_work_time);
//            final TextView txtGoTime =
//                    (TextView) rootView.findViewById(R.id.text_go_time);
//            final TextView txtLeaveTime =
//                    (TextView) rootView.findViewById(R.id.text_leave_time);
//            final ViolationDetail item = getItem(position);
//            final Resources res = getContext().getResources();
//
//            txtWorkTime.setText(res.getString(R.string.report_time_work,
//                    item.TimeWorkStart, item.TimeWorkEnd));
//            txtGoTime.setText(res.getString(R.string.report_time_come, item.TimeCome));
//            txtLeaveTime.setText(res.getString(R.string.report_time_leave, item.TimeLeave));
//
//            return rootView;
//        }
//    }

    private ViewFlipper viewFlipper;
    private LinearLayout layoutViolation;
    private String mPersonId;
    private boolean mStartFromSearch = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_time_person_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.base_toolbar);
        setSupportActionBar(toolbar);

        final Intent intent = getIntent();
        final String action = intent.getStringExtra(ACTION_KEY);
        mStartFromSearch = (action != null && Intent.ACTION_SEARCH.equals(action));
        final Person person = (Person) intent.getSerializableExtra(PERSON_KEY);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(person.Name);

        final Resources res = getResources();

        final TextView txtId = (TextView) findViewById(R.id.text_id_rt);
        final TextView txtName = (TextView) findViewById(R.id.text_name_rt);
        final TextView txtStatus = (TextView) findViewById(R.id.text_status_rt);
        final TextView txtViolationCount = (TextView) findViewById(R.id.text_violate_rt);
        final TextView txtLastScan = (TextView) findViewById(R.id.text_last_scan_rt);

        txtId.setText(person.Code);
        txtName.setText(person.Name);

        String workState = res.getString(R.string.real_time_status_home);
        if (person.WorkState.equals(Person.WORK_STATE_IN_WORK)) {
            workState = res.getString(R.string.real_time_status_in_work);
        } else if (person.WorkState.equals(Person.WORK_STATE_OUT_WORK)) {
            workState = res.getString(R.string.real_time_status_out_work);
        }
        txtStatus.setText(workState);

        txtLastScan.setText(res.getString(R.string.real_time_last_scan, person.LastScan));
        txtViolationCount.setText(res.getString(R.string.real_time_violate,
                person.ViolationCount));

        final Button btnDetail = (Button) findViewById(R.id.button_more_detail);
        btnDetail.setOnClickListener(this);
        if (person.ViolationCount == 0) {
            btnDetail.setVisibility(View.GONE);
        }

        viewFlipper = (ViewFlipper) findViewById(R.id.flipper_rt_detail);
        layoutViolation = (LinearLayout) findViewById(R.id.layout_violation);
        mPersonId = person.Code;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home && mStartFromSearch) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (!GlobalFunctions.checkNetworkAvailable(this)) {
            Toast.makeText(RealTimePersonDetailActivity.this,
                    R.string.can_not_connect, Toast.LENGTH_SHORT).show();
            return;
        }
        else viewFlipper.showNext();

        final Date now = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        final String url = String.format(Locale.US, "/report/person/%s/daily/%s/%s/%s",
                mPersonId, c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH));

        RestClient.get(url, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    final String result = response.getString("result");
                    if (!result.equals("ok"))
                        throw new JSONException("Server return result fail");
                    JSONArray data = response.getJSONObject("data")
                            .getJSONArray("violation_list");

                    for (int i = 0; i < data.length(); i++) {
                        ViolationDetail vd = new ViolationDetail();
                        final JSONObject item = data.getJSONObject(i);
                        vd.TimeWorkStart = item.getString("time_work_start");
                        vd.TimeWorkEnd = item.getString("time_work_end");
                        vd.TimeCome = item.getString("time_come");
                        vd.TimeLeave = item.getString("time_leave");

                        if (i > 0) {
                            layoutViolation.addView(getHorizontalSeparatorView(layoutViolation));
                        }
                        layoutViolation.addView(getView(layoutViolation, vd));
                    }
                    viewFlipper.showNext();
                } catch (JSONException e) {
                    e.printStackTrace();
                    GlobalFunctions.log(e);
                    onRequestMoreDetailFailure();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {

                super.onFailure(statusCode, headers, responseString, throwable);
                onRequestMoreDetailFailure();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {

                super.onFailure(statusCode, headers, throwable, errorResponse);
                onRequestMoreDetailFailure();
            }
        });
    }

    private View getView(ViewGroup parent, ViolationDetail vd) {
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.view_real_time_detail_item, parent, false);

        final TextView txtWorkTime =
                (TextView) rootView.findViewById(R.id.text_work_time);
        final TextView txtGoTime =
                (TextView) rootView.findViewById(R.id.text_go_time);
        final TextView txtLeaveTime =
                (TextView) rootView.findViewById(R.id.text_leave_time);
        final Resources res = getResources();

        txtWorkTime.setText(res.getString(R.string.report_time_work,
                vd.TimeWorkStart, vd.TimeWorkEnd));
        txtGoTime.setText(res.getString(R.string.report_time_come, vd.TimeCome));
        txtLeaveTime.setText(res.getString(R.string.report_time_leave, vd.TimeLeave));

        return rootView;
    }

    private View getHorizontalSeparatorView(ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(R.layout.view_list_separator_horizontal, parent, false);
    }

    private void onRequestMoreDetailFailure() {
        Toast.makeText(RealTimePersonDetailActivity.this,
                R.string.can_not_load_data, Toast.LENGTH_SHORT).show();
        viewFlipper.showPrevious();
    }

    private static class ViolationDetail {

        public String TimeWorkStart;
        public String TimeWorkEnd;
        public String TimeCome;
        public String TimeLeave;
    }
}
