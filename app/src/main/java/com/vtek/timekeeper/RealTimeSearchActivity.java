package com.vtek.timekeeper;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SearchRecentSuggestionsProvider;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.vtek.timekeeper.content.RealTimeSuggestionProvider;
import com.vtek.timekeeper.model.Person;
import com.vtek.timekeeper.app.TimeKeeperBackgroundService;
import com.vtek.timekeeper.utils.GlobalFunctions;
import com.vtek.timekeeper.content.RealTimeRecyclerAdapter;

import java.util.List;


public class RealTimeSearchActivity extends ActionBarActivity
        implements RealTimeRecyclerAdapter.OnItemClickListener {

    private TextView txtSearchResultFail, txtSearchResultCount;
    private ViewFlipper viewFlipper;

    private RealTimeRecyclerAdapter mAdapter;
    private TimeKeeperBackgroundService mBackgroundService;
    private String mQuery = "";
    private int mViewState = VIEW_STATE_CONTENT;

    private static final int VIEW_STATE_CONTENT = 1;
    private static final int VIEW_STATE_PROGRESS = 2;
    private static final int VIEW_STATE_FAIL = 3;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_time_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.base_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewFlipper = (ViewFlipper) findViewById(R.id.view_switcher);
        txtSearchResultFail = (TextView) findViewById(R.id.text_result_fail);
        txtSearchResultCount = (TextView) findViewById(R.id.text_result_count);

        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(linearLayout);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        mAdapter = new RealTimeRecyclerAdapter(this, this);
        recyclerView.setAdapter(mAdapter);

        handleIntent(getIntent());

        Intent i = new Intent(this, TimeKeeperBackgroundService.class);
        bindService(i, mConnection, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mConnection);
        GlobalFunctions.log(getClass() + " destroyed");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_real_time, menu);

        final SearchManager searchManager = (SearchManager)
                getSystemService(Context.SEARCH_SERVICE);

        final MenuItem item = menu.findItem(R.id.action_search);
//        MenuItemCompat.setOnActionExpandListener(item,
//                new MenuItemCompat.OnActionExpandListener() {
//
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem item) {
//                return true;
//            }
//
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem item) {
//                NavUtils.navigateUpTo(RealTimeSearchActivity.this,
//                        NavUtils.getParentActivityIntent(RealTimeSearchActivity.this));
//                return false;
//            }
//        });

        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQuery(mQuery, false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                MenuItemCompat.collapseActionView(item);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setQuery(mQuery, false);
            }
        });
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int i) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int i) {
                MenuItemCompat.collapseActionView(item);
                return false;
            }
        });

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
        handleSearchQuery();
    }

    @Override
    public void onItemClick(RealTimeRecyclerAdapter adapter, View view, int pos, int id) {
        final Person person = adapter.getItemAt(pos);
        Intent intent = new Intent(this, RealTimePersonDetailActivity.class);
        intent.putExtra(RealTimePersonDetailActivity.PERSON_KEY, person);
        intent.putExtra(RealTimePersonDetailActivity.ACTION_KEY, Intent.ACTION_SEARCH);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(
                getApplicationContext(), R.anim.slide_in_right,
                R.anim.slide_out_left
        );
        ActivityCompat.startActivity(this, intent, options.toBundle());
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            mQuery = intent.getStringExtra(SearchManager.QUERY);
            getSupportActionBar().setTitle(mQuery);
            txtSearchResultFail.setText(getResources().getString(
                    R.string.search_result_fail, mQuery));
            GlobalFunctions.log("Received new query: " + mQuery);

            SearchRecentSuggestions suggestion = new SearchRecentSuggestions(
                    getApplicationContext(),
                    RealTimeSuggestionProvider.AUTHORITY, RealTimeSuggestionProvider.MODE);
            suggestion.saveRecentQuery(mQuery, null);

            if (mViewState == VIEW_STATE_CONTENT) {
                viewFlipper.showNext();
            } else if (mViewState == VIEW_STATE_FAIL) {
                viewFlipper.showPrevious();
            }
            mViewState = VIEW_STATE_PROGRESS;
        }
    }

    private void handleSearchQuery() {
        final List<Person> personList = mBackgroundService.findPersonByName(mQuery);
        if (personList.isEmpty()) {
            viewFlipper.showNext();
            mViewState = VIEW_STATE_FAIL;
        } else {
            mAdapter.addAll(personList);
            txtSearchResultCount.setText(getResources().getString(
                    R.string.search_result, personList.size(), mQuery));
            viewFlipper.showPrevious();
            mViewState = VIEW_STATE_CONTENT;
        }
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TimeKeeperBackgroundService.LocalBinder binder =
                    (TimeKeeperBackgroundService.LocalBinder) service;
            mBackgroundService = binder.getService();
            handleSearchQuery();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBackgroundService = null;
        }
    };
}
