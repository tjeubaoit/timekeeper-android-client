package com.vtek.timekeeper.utils;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;

public class GlobalFunctions {

    public static String getCurrentUserNameAuthenticated(Context context) {
        SharedPreferences pref = context.getSharedPreferences(
                GlobalValues.APP_PREF_NAME, Context.MODE_PRIVATE);
        return pref.getString(GlobalValues.USER_KEY, "");
    }

    /**
     *
     * @param context
     * @param title
     * @param msg
     * @param notificationId
     * @param contentIntent
     * @param deleteIntent
     */
    public static void sendNotification(Context context, String title, String msg,
                                        int notificationId, PendingIntent contentIntent,
                                        PendingIntent deleteIntent) {

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(android.R.drawable.ic_secure)
                        .setContentTitle(title)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                        .setContentText(msg)
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(
                                RingtoneManager.TYPE_NOTIFICATION));

        builder.setContentIntent(contentIntent);
        if (deleteIntent != null) builder.setDeleteIntent(deleteIntent);
        notificationManager.notify(notificationId, builder.build());
    }

    /**
     *
     * @param context
     * @param notificationId
     */
    public static void cancelNotification(Context context, int notificationId) {
        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(notificationId);
    }

    /**
     * Check a service is running or not
     * @param serviceClass Name of class contains service to check
     * @return true if service is running or otherwise
     */
    public static boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager =
                (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo serviceInfo : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(serviceInfo.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if application is authenticated, not need to re-login or show login form
     * @return true if app is authenticated or false if otherwise
     */
    public static boolean isApplicationAuthenticated(Context context) {
        final SharedPreferences settings = context.getSharedPreferences(
                GlobalValues.APP_PREF_NAME, Context.MODE_PRIVATE);
        return settings.getBoolean(GlobalValues.AUTHENTICATED_KEY, false);
    }

    public static String encryptPassword(String password) {
        String sha1 = "";
        try
        {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(password.getBytes("UTF-8"));
            sha1 = byteToHex(crypt.digest());
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            GlobalFunctions.log(e);
        }
        return sha1;
    }

    public static String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash) {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }

    public static boolean checkNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = connectivityManager.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getType() == ConnectivityManager.TYPE_WIFI)
                if (ni.isConnected()) GlobalFunctions.log("WIFI connected");
            if (ni.getType() == ConnectivityManager.TYPE_MOBILE)
                if (ni.isConnected()) GlobalFunctions.log("3G connected");
        }
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void log(Object obj) {
        Log.i("TIMEKEEPER-DEBUG", obj.toString());
    }

    public static void error(Object obj) {
        Log.e("TIMEKEEPER-ERROR", obj.toString());
    }
}
