package com.vtek.timekeeper.utils;

public class GlobalValues {

    public static final String APP_PREF_NAME = "com.vtek.timekeeper.pref_name";
    public static final String USER_KEY = "username";
    public static final String PASSWORD_KEY = "password";
    public static final String AUTHENTICATED_KEY = "authenticated";

    public static final String ACTION_GCM_TO_BACKGROUND = "gcm.to.background";
    public static final String ACTION_BACKGROUND_DATA_CHANGED = "background.data.changed";
    public static final String GCM_COLLAPSE_KEY_REALTIME_UPDATE = "realtime.update";

    public static final int HTTP_BAD_REQUEST = 400;
}
