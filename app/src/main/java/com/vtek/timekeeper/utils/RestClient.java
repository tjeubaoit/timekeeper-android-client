package com.vtek.timekeeper.utils;

import android.content.Context;
import android.util.Base64;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.SyncHttpClient;
import com.vtek.timekeeper.utils.GlobalFunctions;

import org.apache.http.HttpEntity;
import org.apache.http.impl.client.DefaultRedirectHandler;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class RestClient {

//    private static final String BASE_URL = "http://api-vtekco.ddns.net";
    private static final String BASE_URL = "http://192.168.2.104";
    private static final String DEFAULT_REALM = "timekeeper@api-vtekco.ddns.net";
    private static final int DEFAULT_HTTP_PORT = 8000;

    private static AsyncHttpClient sAsyncHttp = new AsyncHttpClient(DEFAULT_HTTP_PORT);
    private static SyncHttpClient sSyncHttp = new SyncHttpClient(DEFAULT_HTTP_PORT);

    private static String sUserName;
    private static String sHashPass;
    private static int sNonce = 0;
    private static boolean sFirstSetup = false;

    /**
     *
     * @param url
     * @param params
     * @param responseHandler
     */
    public static void get(String url, RequestParams params,
                           AsyncHttpResponseHandler responseHandler) {

        initRequestHeader(sAsyncHttp, url);
        sAsyncHttp.get(getAbsoluteUrl(url), params, responseHandler);
    }

    /**
     *
     * @param url
     * @param responseHandler
     */
    public static void get(String url, AsyncHttpResponseHandler responseHandler) {
        initRequestHeader(sAsyncHttp, url);
        sAsyncHttp.get(getAbsoluteUrl(url), responseHandler);
    }

    /**
     *
     * @param url
     * @param params
     * @param responseHandler
     */
    public static void getSync(String url, RequestParams params,
                               ResponseHandlerInterface responseHandler) {

        initRequestHeader(sSyncHttp, url);
        sSyncHttp.get(getAbsoluteUrl(url), params, responseHandler);
    }

    /**
     *
     * @param url
     * @param responseHandler
     */
    public static void getSync(String url, ResponseHandlerInterface responseHandler) {
        initRequestHeader(sSyncHttp, url);
        sSyncHttp.get(getAbsoluteUrl(url), responseHandler);
    }

    /**
     *
     * @param url
     * @param params
     * @param responseHandler
     */
    public static void post(Context context, String url, HttpEntity entity,
                            AsyncHttpResponseHandler responseHandler) {

        initRequestHeader(sAsyncHttp, url, "POST");
        sAsyncHttp.post(context, getAbsoluteUrl(url), entity, "application/json",
                responseHandler);
    }

    /**
     *
     * @param url
     * @param params
     * @param responseHandler
     */
    public static void postSync(Context context, String url, HttpEntity entity,
                            AsyncHttpResponseHandler responseHandler) {

        initRequestHeader(sSyncHttp, url, "POST");
        sSyncHttp.post(context, getAbsoluteUrl(url), entity, "application/json",
                responseHandler);
    }

    /**
     *
     * @param name
     * @param pass
     */
    public static void setAuthenticationInfo(String name, String pass) {
        sUserName = name;
        sHashPass = GlobalFunctions.encryptPassword(pass);
    }

    public static void setTimeout(int timeout) {
        sAsyncHttp.setTimeout(timeout);
        sSyncHttp.setTimeout(timeout);
    }

    private static void initRequestHeader(AsyncHttpClient http, String url) {
        initRequestHeader(http, url, "GET", DEFAULT_REALM);
    }

    private static void initRequestHeader(AsyncHttpClient http, String url, String method) {
        initRequestHeader(http, url, method, DEFAULT_REALM);
    }

    private static void initRequestHeader(AsyncHttpClient httpClient, String url,
                                          String method, String realm) {
        if (!sFirstSetup) {
            sFirstSetup = true;
            doFirstSetupGeneral(sSyncHttp);
            doFirstSetupGeneral(sAsyncHttp);
        }
        try {
            int nonce;
            do {
                nonce = new Random().nextInt(Short.MAX_VALUE);
            } while (nonce == sNonce);
            sNonce = nonce;

            final Date expireDate = new Date(System.currentTimeMillis() + 10*60*1000);
            final String expireStr = new SimpleDateFormat("y-M-d H:m:s").format(expireDate);
            final String plainRes = String.format(Locale.US, "%s+%s+%s+%s+%d",
                    sHashPass, method, url, expireStr, sNonce);

            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(plainRes.getBytes("UTF-8"));
            final String encodedRes = Base64.encodeToString(crypt.digest(), Base64.NO_WRAP);
            final String authText = String.format(Locale.US,
                    "Digest username=%s, realm=%s, nonce=%d, uri=%s, response=%s",
                    sUserName, realm, nonce, url, encodedRes);

            httpClient.removeAllHeaders();
            httpClient.addHeader("Date", expireStr);
            httpClient.addHeader("Authorization", authText);
            httpClient.addHeader("Accept", "application/json");
            if (method.equals("POST")) {
                httpClient.addHeader("Content-Type", "application/json");
            }
        } catch (NoSuchAlgorithmException ex) {
            GlobalFunctions.log(ex);
        } catch (UnsupportedEncodingException ex) {
            GlobalFunctions.log(ex);
        }
    }

    private static String getAbsoluteUrl(String relativeUrl) {

        return BASE_URL + relativeUrl;
    }

    private static void doFirstSetupGeneral(AsyncHttpClient httpClient) {
        httpClient.setEnableRedirects(true);
        httpClient.setUserAgent("Mozilla/5.0 (Windows NT 6.3; WOW64) " +
                "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.94 Safari/537.36");
        httpClient.setURLEncodingEnabled(true);
        httpClient.setRedirectHandler(new DefaultRedirectHandler());
    }
}
