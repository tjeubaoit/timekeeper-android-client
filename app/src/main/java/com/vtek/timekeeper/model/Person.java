package com.vtek.timekeeper.model;

import java.io.Serializable;

public class Person implements Serializable {

    public static final String WORK_STATE_HOME = "home";
    public static final String WORK_STATE_IN_WORK = "in_work";
    public static final String WORK_STATE_OUT_WORK = "out_work";

    public int ID;
    public String Code;
    public String Name;
    public String Birthday;
    public String WorkState;
    public int ViolationCount;
    public String LastScan;

    public boolean equals(Object o) {
        Person person = (Person) o;
        return Code == person.Code
                && Name == person.Name
                && Birthday == person.Birthday
                && WorkState == person.WorkState
                && ViolationCount == person.ViolationCount
                && LastScan == person.LastScan;
    }
}
