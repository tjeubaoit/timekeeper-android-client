package com.vtek.timekeeper.model;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.vtek.timekeeper.utils.RestClient;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;
import org.lib.anhtn.gcm.AbstractGcmRegister;

import java.io.UnsupportedEncodingException;

public class GcmRegister extends AbstractGcmRegister {

    private static final String TAG = "GCM-REGISTER-DEBUG";
    private static final String SENDER_ID = "973865824950";

    private static GcmRegister sInstance = null;

    public static GcmRegister getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new GcmRegister(context, SENDER_ID);
        }
        return sInstance;
    }

    private GcmRegister(Context context, String senderId) {
        super(context, senderId);
    }

    private boolean mBackendResultSuccess = false;

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */
    protected boolean sendRegistrationIdToBackend() {
        // Your implementation here.
        mBackendResultSuccess = false;
        try {
            JSONObject data = new JSONObject();
            data.put("gcm_id", getRegistrationId());
            HttpEntity entity = new StringEntity(data.toString());

            RestClient.postSync(getContext(), "/gcm/register", entity,
                    new JsonHttpResponseHandler() {

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            super.onSuccess(statusCode, headers, response);
                            Log.e(TAG, "Status code " + statusCode);
                            Log.e(TAG, "Send GCM registrationId to backend success");
                            mBackendResultSuccess = true;
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString,
                                              Throwable throwable) {

                            super.onFailure(statusCode, headers, responseString, throwable);
                            Log.e(TAG, "Status code " + statusCode);
                            Log.e(TAG, "Send GCM registrationId to backend failed");
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                              JSONObject errorResponse) {

                            super.onFailure(statusCode, headers, throwable, errorResponse);
                            Log.e(TAG, "Status code " + statusCode);
                            Log.e(TAG, "Send GCM registrationId to backend failed");
                        }
                    });
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }

        return mBackendResultSuccess;
    }
}
