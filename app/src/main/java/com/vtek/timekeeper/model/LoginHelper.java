package com.vtek.timekeeper.model;

import android.content.Context;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.vtek.timekeeper.utils.GlobalFunctions;
import com.vtek.timekeeper.utils.RestClient;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class LoginHelper extends JsonHttpResponseHandler {

    private static final int DEFAULT_LOGIN_TIMEOUT = 10000;

    public static interface Callback {

        public void onPrepareLogin();
        public void onLoginSuccess(String user, String pass);
        public void onLoginFailure(String user, String pass);
    }

    private Callback mCallback;
    private String mUserName, mPassword;

    public LoginHelper(String username, String password, Callback callback) {
        if (callback == null) {
            throw new NullPointerException("Callback cannot be null");
        } else {
            mUserName = username;
            mPassword = password;
            mCallback = callback;

            RestClient.setTimeout(DEFAULT_LOGIN_TIMEOUT);
        }
    }

    public void asyncLogin(Context context) {
        mCallback.onPrepareLogin();

        final String gcmId = GcmRegister.getInstance(context).getRegistrationId();
        final HttpEntity entity;
        try {
            JSONObject data = new JSONObject();
            data.put("gcm_id", gcmId);
            entity = new StringEntity(data.toString());
        } catch (JSONException | UnsupportedEncodingException e) {
            e.printStackTrace();
            mCallback.onLoginFailure(mUserName, mPassword);
            return;
        }

        RestClient.setAuthenticationInfo(mUserName, mPassword);
        RestClient.post(context, "/login/" + mUserName, entity, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                super.onSuccess(statusCode, headers, response);
                if (response != null) {
                    GlobalFunctions.log("Response: " + response.toString());
                }
                try {
                    if (response != null && response.get("result").equals("ok")) {
                        GlobalFunctions.log("Login success");
                        mCallback.onLoginSuccess(mUserName, mPassword);
                        return;
                    }
                } catch (JSONException e) {
                    GlobalFunctions.log(e);
                }
                mCallback.onLoginFailure(mUserName, mPassword);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString,
                                  Throwable throwable) {

                super.onFailure(statusCode, headers, responseString, throwable);
                GlobalFunctions.log("Login failure. Status code " + statusCode);
                GlobalFunctions.log(String.format("Response: %s", responseString));
                mCallback.onLoginFailure(mUserName, mPassword);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable,
                                  JSONObject errorResponse) {

                super.onFailure(statusCode, headers, throwable, errorResponse);
                GlobalFunctions.log("Login failure. Status code " + statusCode);
                if (errorResponse != null) {
                    GlobalFunctions.log("Response: " + errorResponse.toString());
                }
                mCallback.onLoginFailure(mUserName, mPassword);
            }
        });
    }

    public void setLoginTimeout(int timeout) {
        RestClient.setTimeout(timeout);
    }
}
