package com.vtek.timekeeper.content;

import android.content.SearchRecentSuggestionsProvider;

public class RealTimeSuggestionProvider extends SearchRecentSuggestionsProvider {

    public final static String AUTHORITY = "com.vtek.timekeeper.RealTimeSuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public RealTimeSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
