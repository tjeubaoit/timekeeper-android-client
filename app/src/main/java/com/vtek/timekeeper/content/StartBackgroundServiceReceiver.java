package com.vtek.timekeeper.content;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vtek.timekeeper.app.TimeKeeperBackgroundService;
import com.vtek.timekeeper.utils.GlobalFunctions;

public class StartBackgroundServiceReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Intent i = new Intent(context, TimeKeeperBackgroundService.class);
        context.startService(i);
        GlobalFunctions.log("Start background service");
    }
}
