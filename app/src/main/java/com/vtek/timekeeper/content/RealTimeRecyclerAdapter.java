package com.vtek.timekeeper.content;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.vtek.timekeeper.R;
import com.vtek.timekeeper.model.Person;

import java.util.ArrayList;
import java.util.List;

public class RealTimeRecyclerAdapter
        extends RecyclerView.Adapter<RealTimeRecyclerAdapter.ViewHolder> {

    public static interface OnItemClickListener {

        public void onItemClick(RealTimeRecyclerAdapter adapter, View view,
                                int position, int id);
    }

    /**
     * Provide a reference to the views for each data item
     * Complex data items may need more than one view per item, and
     * you provide access to all the views for a data item in a view holder
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public View rootView, headerView;
        public TextView txtStatus, txtViolate, txtLastScan;
        public TextView txtId, txtName;
        public ImageView imgAvatar;
        public ImageButton btnDetail;

        public ViewHolder(View v) {
            super(v);
            rootView = v;

            txtStatus = (TextView) rootView.findViewById(R.id.text_status_rt);
            txtViolate = (TextView) rootView.findViewById(R.id.text_violate_rt);
            txtLastScan = (TextView) rootView.findViewById(R.id.text_last_scan_rt);
            txtId = (TextView) rootView.findViewById(R.id.text_id_rt);
            txtName = (TextView) rootView.findViewById(R.id.text_name_rt);
            imgAvatar = (ImageView) rootView.findViewById(R.id.img_avatar_rt);
            headerView = rootView.findViewById(R.id.real_time_item_header);
            btnDetail = (ImageButton) rootView.findViewById(R.id.button_detail_rt);
        }
    }

    private List<Person> mDataset;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;

    /**
     * Provide a suitable constructor (depends on the kind of dataset)
     * Init data set with a empty list
     * @param context
     */
    public RealTimeRecyclerAdapter(Context context,
                                   OnItemClickListener onClickListener) {
        mContext = context;
        mDataset = new ArrayList<>();
        mOnItemClickListener = onClickListener;
    }

    public void addAll(List<Person> persons) {
        mDataset.clear();
        mDataset.addAll(persons);
        notifyDataSetChanged();
    }

    public void updateItem(Person person) {
        int position = -1;
        for (int i = 0; i < mDataset.size(); i++) {
            if (mDataset.get(i).ID == person.ID) {
                position = i;
                break;
            }
        }
        if (position >= 0) {
            mDataset.set(position, person);
            notifyItemChanged(position);
        }
    }

    public void addItem(Person person) {
        mDataset.add(person);
        notifyItemInserted(mDataset.size() - 1);
    }

    public void removeItem(int position) {
        mDataset.remove(position);
        notifyItemRemoved(position);
    }

    public Person getItemAt(int position) {
        return mDataset.get(position);
    }

    public Person findItemById(int id) {
        for (Person person : mDataset) {
            if (person.ID == id) return person;
        }
        return null;
    }

    /**
     * Create new views (invoked by the layout manager)
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        final View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_real_time_item, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder holder = new ViewHolder(view);
        holder.btnDetail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final TextView textView = (TextView) view.findViewById(R.id.text_id_rt);
                int pos = -1;
                for (int i = 0; i < mDataset.size(); i++) {
                    if (mDataset.get(i).Code.equals(textView.getText())) {
                        pos = i;
                        break;
                    }
                }
                if (pos >= 0) {
                    mOnItemClickListener.onItemClick(RealTimeRecyclerAdapter.this,
                            view, pos, mDataset.get(pos).ID);
                }
            }
        });
        return holder;
    }

    /**
     * Replace the contents of a view (invoked by the layout manager)s
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Person person = mDataset.get(position);
        holder.txtId.setText(person.Code);
        holder.txtName.setText(person.Name);

        final Resources res = mContext.getResources();

        String workState = res.getString(R.string.real_time_status_home);
        holder.headerView.setBackgroundResource(R.drawable.real_time_header_grey);
        if (person.WorkState.equals(Person.WORK_STATE_IN_WORK)) {
            workState = res.getString(R.string.real_time_status_in_work);
            holder.headerView.setBackgroundResource(R.drawable.real_time_header_green);
        } else if (person.WorkState.equals(Person.WORK_STATE_OUT_WORK)) {
            workState = res.getString(R.string.real_time_status_out_work);
            holder.headerView.setBackgroundResource(R.drawable.real_time_header_red);
        }
        holder.txtStatus.setText(res.getString(R.string.real_time_status, workState));

        holder.txtViolate.setText(res.getString(R.string.real_time_violate,
                person.ViolationCount));
        holder.txtLastScan.setText(res.getString(R.string.real_time_last_scan,
                person.LastScan));
    }

    /**
     * Return the size of your dataset (invoked by the layout manager)
     * @return
     */
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
