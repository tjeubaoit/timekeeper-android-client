package com.vtek.timekeeper.content;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.vtek.timekeeper.model.Person;
import com.vtek.timekeeper.utils.RestClient;
import com.vtek.timekeeper.utils.GlobalFunctions;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RealTimeLoader extends AsyncTaskLoader<List<Person>> {

    private List<Person> mPersons;
    private String mUserName;

    /**
     * Stores away the application context associated with context. Since Loaders can be used
     * across multiple activities it's dangerous to store the context directly.
     *
     * @param context used to retrieve the application context.
     */
    public RealTimeLoader(Context context, String userName) {
        super(context);
        mUserName = userName;
    }

    @Override
    public List<Person> loadInBackground() {
        RestClient.getSync("/realtime/" + mUserName, new JsonHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    if (!response.getString("result").equals("ok")) return;
                    JSONArray array = response.getJSONArray("persons");
                    mPersons = new ArrayList<>(array.length());

                    for (int i = 0; i < array.length(); i++) {
                        final JSONObject obj = array.getJSONObject(i);
                        Person person = new Person();
                        person.ID = obj.getInt("id");
                        person.Code = obj.getString("code");
                        person.Name = obj.getString("name");
                        person.ViolationCount = obj.getInt("violation_count");
                        person.WorkState = obj.getString("work_state");

                        if (obj.has("birthday"))
                            person.Birthday = obj.getString("birthday");
                        if (obj.has("last_scan"))
                            person.LastScan = obj.getString("last_scan");

                        mPersons.add(person);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    GlobalFunctions.log(e);
                }
            }
        });
        return mPersons;
    }

    @Override
    public void deliverResult(List<Person> data) {
        if (isReset()) {
            // The Loader has been reset; ignore the result and invalidate the data.
            return;
        }

        // Hold a reference to the old data so it doesn't get garbage collected.
        // The old data may still be in use (i.e. bound to an adapter, etc.), so
        // we must protect it until the new data has been delivered.
        List<Person> oldData = mPersons;
        mPersons = data;

        if (isStarted()) {
            // If the Loader is in a started state, deliver the results to the
            // client. The superclass method does this for us.
            super.deliverResult(data);
        }

        // Invalidate the old data as we don't need it any more.
        if (oldData != null && oldData != data) {
            oldData = null;
        }
    }

    @Override
    protected void onStartLoading() {
        if (mPersons != null) {
            // Deliver any previously loaded data immediately.
            deliverResult(mPersons);
        }
        if (takeContentChanged() || mPersons == null) {
            // When the observer detects a change, it should call onContentChanged()
            // on the Loader, which will cause the next call to takeContentChanged()
            // to return true. If this is ever the case (or if the current data is
            // null), we force a new load.
            forceLoad();
        }
        GlobalFunctions.log("Loader start loading");
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
        GlobalFunctions.log("Loader stop loading");
    }
}
