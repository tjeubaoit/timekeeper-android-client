package com.vtek.timekeeper;

import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.vtek.timekeeper.app.BaseRealTimeActivity;
import com.vtek.timekeeper.app.NavigationDrawerFragment;
import com.vtek.timekeeper.app.TimeKeeperBackgroundService;
import com.vtek.timekeeper.content.RealTimeLoader;
import com.vtek.timekeeper.content.RealTimeRecyclerAdapter;
import com.vtek.timekeeper.model.Person;
import com.vtek.timekeeper.utils.GlobalFunctions;
import com.vtek.timekeeper.utils.GlobalValues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class RealTimeActivity extends BaseRealTimeActivity
        implements LoaderManager.LoaderCallbacks<List<Person>>,
        NavigationDrawerFragment.NavigationEventListener {

    public static final int NOTIFICATION_ID = 0xabcd;

    private static final int STATE_LOADING = 0x0010;
    private static final int STATE_ERROR_NETWORK = 0x0011;
    private static final int STATE_ERROR_UNKNOWN = 0x0012;
    private static final int STATE_OK = 0x0013;

    private static final int VIEW_TYPE_ALL = 0;
    private static final int VIEW_TYPE_ONLY_IN = 1;
    private static final int VIEW_TYPE_ONLY_OUT = 2;
    private static final int VIEW_TYPE_ONLY_HOME = 3;

    private TimeKeeperBackgroundService mBackgroundService;
    private LoaderManager mLoaderManager;

    private ProgressBar progressBar;
    private View layoutError;
    private TextView textErrorContent;
    private Spinner spinner;
    private MenuItem menuItemSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoaderManager = getSupportLoaderManager();

        final ActionBar ab = getSupportActionBar();
        ab.setDisplayShowTitleEnabled(false);
        ab.setTitle("");

        spinner = (Spinner) getToolbar().findViewById(R.id.spinner_nav_real_time);
        final String[] viewTypes = getResources().getStringArray(
                R.array.real_time_spinner_items);
        BaseAdapter adapter = new ViewTypeSpinnerAdapter(Arrays.asList(viewTypes));
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {
                mAdapter.addAll(getPersonListByViewType(pos));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // do nothing
            }
        });

        layoutError = findViewById(R.id.real_time_error_form);
        layoutError.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    startLoadData();
                    return true;
                }
                return false;
            }
        });

        progressBar = (ProgressBar) findViewById(R.id.progress_real_time);
        textErrorContent = (TextView) findViewById(R.id.text_load_error_content);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, TimeKeeperBackgroundService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sActivityVisible = true;

        final IntentFilter filter =
                new IntentFilter(GlobalValues.ACTION_BACKGROUND_DATA_CHANGED);
        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mBackgroundDataChangedReceiver, filter);

        if (menuItemSearch != null) {
            MenuItemCompat.collapseActionView(menuItemSearch);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        sActivityVisible = false;

        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mBackgroundDataChangedReceiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mConnection);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        GlobalFunctions.log("New intent: " + intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow
            GlobalFunctions.log("Search query: " + query);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!getNavigationDrawerFragment().isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.menu_real_time, menu);

            final SearchManager searchManager = (SearchManager)
                    getSystemService(Context.SEARCH_SERVICE);
            menuItemSearch = menu.findItem(R.id.action_search);

            final SearchView searchView = (SearchView)
                    MenuItemCompat.getActionView(menuItemSearch);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                @Override
                public boolean onQueryTextSubmit(String s) {
                    Intent i = new Intent(RealTimeActivity.this, RealTimeSearchActivity.class);
                    i.putExtra(SearchManager.QUERY, s);
                    i.setAction(Intent.ACTION_SEARCH);
                    startActivity(i);
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String s) {
                    return false;
                }
            });

            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_search) {
//            Intent i = new Intent(this, SearchActivity.class);
//            startActivity(i);
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationItemSelected(int id) {
        switch (id) {
            case NavigationDrawerFragment.NavigationEventListener.ABOUT_ITEM_ID: {
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
            }

            case NavigationDrawerFragment.NavigationEventListener.HELP_FEEDBACK_ITEM_ID: {
                Intent intent = new Intent(this, HelpAndFeedBackActivity.class);
                startActivity(intent);
                break;
            }
        }
    }

    @Override
    public int getDefaultSelectedId() {
        return NavigationDrawerFragment.NavigationEventListener.REAL_TIME_ITEM_ID;
    }

    @Override
    public void onItemClick(RealTimeRecyclerAdapter adapter, View v, int pos, int id) {
        final Person person = adapter.getItemAt(pos);
        Intent intent = new Intent(this, RealTimePersonDetailActivity.class);
        intent.putExtra(RealTimePersonDetailActivity.PERSON_KEY, person);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeCustomAnimation(
                getApplicationContext(), R.anim.slide_in_right,
                R.anim.slide_out_left
        );
        ActivityCompat.startActivity(this, intent, options.toBundle());
    }

    @Override
    public Loader<List<Person>> onCreateLoader(int id, Bundle args) {
        final String userName = GlobalFunctions.getCurrentUserNameAuthenticated(this);
        if (userName.isEmpty() && BuildConfig.DEBUG) throw new AssertionError();

        return new RealTimeLoader(this, userName);
    }

    @Override
    public void onLoadFinished(Loader<List<Person>> loader, List<Person> data) {
        if (data != null) {
            mAdapter.addAll(data);
            mBackgroundService.updateNewPersonList(data);
            updateViewVisibility(STATE_OK);
        }
        else {
            recyclerView.postDelayed(new Runnable() {
                @Override
                public void run() {
                    GlobalFunctions.log("Loader error, data is null");
                    updateViewVisibility(STATE_ERROR_UNKNOWN);
                }
            }, 300);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Person>> loader) {
        GlobalFunctions.log("on loader reset");
    }

    @Override
    protected int getToolbarLayoutResId() {
        return R.layout.view_toolbar_real_time;
    }

    private void startLoadData() {
        updateViewVisibility(STATE_LOADING);
        if (!GlobalFunctions.checkNetworkAvailable(this)) {
            layoutError.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateViewVisibility(STATE_ERROR_NETWORK);
                }
            }, 300);
        } else {
            if (mLoaderManager.getLoader(0) == null) {
                mLoaderManager.initLoader(0, null, this);
            } else {
                mLoaderManager.restartLoader(0, null, this);
            }
        }
    }

    private void updateViewVisibility(int state) {
        switch (state) {
            case STATE_LOADING:
                progressBar.setVisibility(View.VISIBLE);
                layoutError.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                break;

            case STATE_ERROR_UNKNOWN:
                textErrorContent.setText(R.string.can_not_load_data);
                layoutError.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                break;

            case STATE_ERROR_NETWORK:
                textErrorContent.setText(R.string.can_not_connect);
                layoutError.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.GONE);
                break;

            case STATE_OK:
                progressBar.setVisibility(View.GONE);
                layoutError.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                break;

            default: break;
        }
    }

    private List<Person> getPersonListByViewType(int viewType) {
        if (viewType == VIEW_TYPE_ALL) {
            return mBackgroundService.getPersonList();
        }
        final String state;
        switch (viewType) {
            case VIEW_TYPE_ONLY_HOME:
                state = Person.WORK_STATE_HOME;
                break;

            case VIEW_TYPE_ONLY_IN:
                state = Person.WORK_STATE_IN_WORK;
                break;

            case VIEW_TYPE_ONLY_OUT:
                state = Person.WORK_STATE_OUT_WORK;
                break;

            default:
                throw new AssertionError("View type invalid: " + viewType);
        }
        List<Person> result = new ArrayList<>();
        for (Person person : mBackgroundService.getPersonList()) {
            if (person.WorkState.equals(state)) {
                result.add(person);
            }
        }
        return result;
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            TimeKeeperBackgroundService.LocalBinder binder =
                    (TimeKeeperBackgroundService.LocalBinder) service;
            mBackgroundService = binder.getService();

            if (mBackgroundService.isDataInitialized()) {
                GlobalFunctions.log("Data is initialized in background");
                if (mBackgroundService.hasDataNeedUpdateInUi()) {
                    final int currentViewType = spinner.getSelectedItemPosition();
                    mAdapter.addAll(getPersonListByViewType(currentViewType));
                }
                updateViewVisibility(STATE_OK);
            } else {
                GlobalFunctions.log("Data is not initialized");
                startLoadData();
            }

            mBackgroundService.setAllItemIsUpdatedInUi();
            GlobalFunctions.cancelNotification(RealTimeActivity.this, NOTIFICATION_ID);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mBackgroundService = null;
        }
    };

    private BroadcastReceiver mBackgroundDataChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final int[] personIds = intent.getIntArrayExtra("person_ids");
            for (final int id : personIds) {
                mAdapter.updateItem(mBackgroundService.findPersonById(id));
            }

            final Resources res = getResources();
            String text;
            if (personIds.length == 1) {
                text = res.getString(R.string.real_time_single_updated,
                        mBackgroundService.findPersonById(personIds[0]).Name);
                Toast.makeText(RealTimeActivity.this, text, Toast.LENGTH_SHORT).show();
            } else if (personIds.length > 1) {
                text = res.getString(R.string.real_time_multi_updated,
                        personIds.length);
            } else return;
            Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
        }
    };

    private class ViewTypeSpinnerAdapter extends BaseAdapter {

        private List<String> mItems = new ArrayList<>();

        public ViewTypeSpinnerAdapter(List<String> items) {
            mItems.addAll(items);
        }

        @Override
        public View getDropDownView(int position, View view, ViewGroup parent) {
            if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
                view = getLayoutInflater().inflate(
                        R.layout.view_toolbar_spinner_dropdown, parent, false);
                view.setTag("DROPDOWN");
            }

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText((String) getItem(position));

            return view;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
                view = getLayoutInflater().inflate(
                        R.layout.view_toolbar_spinner_item, parent, false);
                view.setTag("NON_DROPDOWN");
            }
            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText((String) getItem(position));
            return view;
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    /* true if has at least one instance of this activity on foreground */
    private static boolean sActivityVisible = false;

    /**
     * To global check this activity is foreground or not
     * @return value of static field sActivityVisible
     */
    public static boolean isActivityVisible() {
        return sActivityVisible;
    }
}
