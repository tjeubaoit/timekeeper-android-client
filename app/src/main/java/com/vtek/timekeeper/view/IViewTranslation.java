package com.vtek.timekeeper.view;

public interface IViewTranslation {

    /**
     *
     * @param dx
     */
    public void setTranslationX(float dx);

    /**
     *
     * @param dx
     * @param duration
     */
    public void setTranslationXWithAnimation(float dx, long duration);

    /**
     *
     * @param dy
     */
    public void setTranslationY(float dy);

    /**
     *
     * @param dy
     * @param duration
     */
    public void setTranslationYWithAnimation(float dy, long duration);

    public float getTranslationX();

    public float getTranslationY();

}
