package com.vtek.timekeeper.view;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;

import com.nineoldandroids.view.ViewHelper;
import com.nineoldandroids.view.ViewPropertyAnimator;

public class SystemViewTranslationCompat implements IViewTranslation {

    private static class NineOldTranslation implements IViewTranslation {

        private View mView;

        public NineOldTranslation(View v) {
            mView = v;
        }

        @Override
        public void setTranslationX(float dx) {
            ViewHelper.setTranslationX(mView, dx);
        }

        @Override
        public void setTranslationXWithAnimation(float dx, long duration) {
            ViewPropertyAnimator.animate(mView).translationX(dx).setDuration(duration).start();
        }

        @Override
        public void setTranslationY(float dy) {
            ViewHelper.setTranslationY(mView, dy);
        }

        @Override
        public void setTranslationYWithAnimation(float dy, long duration) {
            ViewPropertyAnimator.animate(mView).translationY(dy).setDuration(duration).start();
        }

        @Override
        public float getTranslationX() {
            return ViewHelper.getTranslationX(mView);
        }

        @Override
        public float getTranslationY() {
            return ViewHelper.getTranslationY(mView);
        }
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public class ICSTranslation implements IViewTranslation {

        private View mView;

        public ICSTranslation(View v) {
            mView = v;
        }

        @Override
        public void setTranslationX(float dx) {
            mView.setTranslationX(dx);
        }

        @Override
        public void setTranslationXWithAnimation(float dx, long duration) {
            mView.animate().translationX(dx).setDuration(duration).start();
        }

        @Override
        public void setTranslationY(float dy) {
            mView.setTranslationY(dy);
        }

        @Override
        public void setTranslationYWithAnimation(float dy, long duration) {
            mView.animate().translationY(dy).setDuration(duration).start();
        }

        @Override
        public float getTranslationX() {
            return mView.getTranslationX();
        }

        @Override
        public float getTranslationY() {
            return mView.getTranslationY();
        }
    }

    private IViewTranslation impl;

    public SystemViewTranslationCompat(View v) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            impl = new ICSTranslation(v);
        } else {
            impl = new NineOldTranslation(v);
        }
    }

    @Override
    public void setTranslationX(float dx) {
        impl.setTranslationX(dx);
    }

    @Override
    public void setTranslationXWithAnimation(float dx, long duration) {
        impl.setTranslationXWithAnimation(dx, duration);
    }

    @Override
    public void setTranslationY(float dy) {
        impl.setTranslationY(dy);
    }

    @Override
    public void setTranslationYWithAnimation(float dy, long duration) {
        impl.setTranslationYWithAnimation(dy, duration);
    }

    @Override
    public float getTranslationX() {
        return impl.getTranslationX();
    }

    @Override
    public float getTranslationY() {
        return impl.getTranslationY();
    }
}
