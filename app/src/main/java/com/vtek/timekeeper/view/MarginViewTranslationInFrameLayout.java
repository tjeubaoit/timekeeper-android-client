package com.vtek.timekeeper.view;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.widget.FrameLayout;

public class MarginViewTranslationInFrameLayout implements IViewTranslation {

    private static interface ITranslationWithAnim {

        /**
         *
         * @param consumer
         * @param start
         * @param end
         * @param duration
         */
        public void animateX(final MarginViewTranslationInFrameLayout consumer,
                             float start, float end, long duration);

        /**
         *
         * @param consumer
         * @param start
         * @param end
         * @param duration
         */
        public void animateY(final MarginViewTranslationInFrameLayout consumer,
                             float start, float end, long duration);
    }

    private static class NineOldTranslationWithAnim implements ITranslationWithAnim {

        @Override
        public void animateX(final MarginViewTranslationInFrameLayout consumer,
                             float start, float end, long duration) {

            com.nineoldandroids.animation.ValueAnimator valueAnimator =
                    com.nineoldandroids.animation.ValueAnimator.ofFloat(start, end);
            valueAnimator.setDuration(duration);
            valueAnimator.addUpdateListener(
                    new com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener() {

                        @Override
                        public void onAnimationUpdate(
                                com.nineoldandroids.animation.ValueAnimator animation) {

                            consumer.setTranslationX((Float) animation.getAnimatedValue());
                        }
                    });
            valueAnimator.start();
        }

        @Override
        public void animateY(final MarginViewTranslationInFrameLayout consumer,
                             float start, float end, long duration) {

            com.nineoldandroids.animation.ValueAnimator valueAnimator =
                    com.nineoldandroids.animation.ValueAnimator.ofFloat(start, end);
            valueAnimator.setDuration(duration);
            valueAnimator.addUpdateListener(
                    new com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener() {

                        @Override
                        public void onAnimationUpdate(
                                com.nineoldandroids.animation.ValueAnimator animation) {

                            consumer.setTranslationY((Float) animation.getAnimatedValue());
                        }
            });
            valueAnimator.start();
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static class HoneyCombTranslationWithAnim implements ITranslationWithAnim {

        @Override
        public void animateX(final MarginViewTranslationInFrameLayout consumer,
                             float start, float end, long duration) {

            ValueAnimator valueAnimator =
                    ValueAnimator.ofFloat(start, end);
            valueAnimator.setDuration(duration);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    consumer.setTranslationX((Float) animation.getAnimatedValue());
                }
            });
            valueAnimator.start();
        }

        @Override
        public void animateY(final MarginViewTranslationInFrameLayout consumer,
                             float start, float end, long duration) {

            ValueAnimator valueAnimator =
                    ValueAnimator.ofFloat(start, end);
            valueAnimator.setDuration(duration);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    consumer.setTranslationY((Float) animation.getAnimatedValue());
                }
            });
            valueAnimator.start();
        }
    }

    private View mView;
    private float mCurrentTranslateX = 0;
    private float mCurrentTranslateY = 0;
    private int mOriginalMarginTop, mOriginalMarginLeft;
    private ITranslationWithAnim animIMPL;

    public MarginViewTranslationInFrameLayout(View v) {
        mView = v;
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mView.getLayoutParams();
        mOriginalMarginLeft = lp.leftMargin;
        mOriginalMarginTop = lp.topMargin;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            animIMPL = new HoneyCombTranslationWithAnim();
        } else {
            animIMPL = new NineOldTranslationWithAnim();
        }
    }

    @Override
    public void setTranslationX(float dx) {
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mView.getLayoutParams();
        lp.leftMargin = mOriginalMarginLeft + (int)dx;
        mView.setLayoutParams(lp);
        mCurrentTranslateX = dx;
    }

    @Override
    public void setTranslationXWithAnimation(float dx, long duration) {
        animIMPL.animateX(this, mCurrentTranslateX, dx, duration);
        mCurrentTranslateX = dx;
    }

    @Override
    public void setTranslationY(float dy) {
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) mView.getLayoutParams();
        lp.topMargin = mOriginalMarginTop + (int) dy;
        mView.setLayoutParams(lp);
        mCurrentTranslateY = dy;
    }

    @Override
    public void setTranslationYWithAnimation(float dy, long duration) {
        animIMPL.animateY(this, mCurrentTranslateY, dy, duration);
        mCurrentTranslateY = dy;
    }

    public float getTranslationX() {
        return mCurrentTranslateX;
    }

    public float getTranslationY() {
        return mCurrentTranslateY;
    }
}
