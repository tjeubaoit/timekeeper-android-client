package com.vtek.timekeeper;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.vtek.timekeeper.model.LoginHelper;
import com.vtek.timekeeper.app.TimeKeeperBackgroundService;
import com.vtek.timekeeper.utils.GlobalFunctions;
import com.vtek.timekeeper.utils.GlobalValues;


public class SplashScreenActivity extends ActionBarActivity {

    private static final int LOGIN_TIMEOUT = 1500;
    private static final int LOGIN_REQUEST_CODE = 211;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (!GlobalFunctions.isServiceRunning(this, TimeKeeperBackgroundService.class)) {
            GlobalFunctions.log("Background service isn't running. Start it");
            Intent i = new Intent(this, TimeKeeperBackgroundService.class);
            startService(i);
        }

        if (GlobalFunctions.isApplicationAuthenticated(this)) {
            startMainTaskAndFinish();
            return;
        }

        final SharedPreferences settings = getSharedPreferences(
                GlobalValues.APP_PREF_NAME, MODE_PRIVATE);
        final String user = settings.getString(GlobalValues.USER_KEY, "");
        final String pass = settings.getString(GlobalValues.PASSWORD_KEY, "");
        if (user.isEmpty() || pass.isEmpty()) {
            startLoginForResult();
            return;
        }

        LoginHelper loginHelper = new LoginHelper(user, pass, new LoginHelper.Callback() {

            @Override
            public void onPrepareLogin() {
            }

            @Override
            public void onLoginSuccess(String user, String pass) {
                startMainTaskAndFinish();
            }

            @Override
            public void onLoginFailure(String user, String pass) {
                startLoginForResult();
            }
        });
        loginHelper.setLoginTimeout(LOGIN_TIMEOUT);
        loginHelper.asyncLogin(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGIN_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                startMainTaskAndFinish();
            } else {
                finish();
            }
        }
    }

    private void startMainTaskAndFinish() {
        Intent intent = new Intent(this, RealTimeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void startLoginForResult() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivityForResult(intent, LOGIN_REQUEST_CODE);
    }
}
