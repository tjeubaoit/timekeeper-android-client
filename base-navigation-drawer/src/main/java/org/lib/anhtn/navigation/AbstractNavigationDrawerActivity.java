package org.lib.anhtn.navigation;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

public abstract class AbstractNavigationDrawerActivity extends ActionBarActivity
        implements BaseNavigationDrawerFragment.Callbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private BaseNavigationDrawerFragment mNavigationDrawerFragment;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_base_navigation_drawer);

        ViewGroup toolbarContainer = (ViewGroup) findViewById(R.id.toolbar_container);
        mToolbar = (Toolbar) getLayoutInflater().inflate(
                getToolbarLayoutResId(), toolbarContainer, false);
        toolbarContainer.removeAllViews();
        toolbarContainer.addView(mToolbar);

        mNavigationDrawerFragment = getNavigationDrawerFragment();
        getSupportFragmentManager().beginTransaction().replace(
                R.id.fragment_navigation_container,
                mNavigationDrawerFragment).commit();

        // Set up the drawer.
        setSupportActionBar(getToolbar());
    }

    @Override
    public void onFragmentAttached() {
        mNavigationDrawerFragment.setUp(R.id.fragment_navigation_container,
                (DrawerLayout) findViewById(R.id.drawer_layout), getToolbar());
    }

    @Override
    public void setContentView(View view) {
        final ViewGroup viewGroup = (ViewGroup) findViewById(R.id.content_container);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {
        final ViewGroup viewGroup = (ViewGroup) findViewById(R.id.content_container);
        viewGroup.removeAllViews();
        viewGroup.setLayoutParams(params);
        viewGroup.addView(view);
    }

    @Override
    public void setContentView(int layoutResID) {
        final ViewGroup viewGroup = (ViewGroup) findViewById(R.id.content_container);
        viewGroup.removeAllViews();
        getLayoutInflater().inflate(layoutResID, viewGroup);
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }

    /**
     * Abstract method that create a fragment implements for navigation drawer
     * @return new BaseNavigationDrawerFragment object or sub-class object
     */
    protected abstract BaseNavigationDrawerFragment getNavigationDrawerFragment();

    protected int getToolbarLayoutResId() {
        return R.layout.view_toolbar_base;
    }
}
